import argparse
import json
import logging
import sys
import time
import traceback

import requests
from requests.adapters import HTTPAdapter

from .providers import PROVIDERS
from .providers.utils import get_connected_ssids

parser = argparse.ArgumentParser(
    prog="waybar-trains",
    description="display information from a train's WiFi network on your Waybar",
)

parser.add_argument(
    "--continuous",
    action="store_true",
    help="continuously output new data; every --interval seconds",
)

parser.add_argument(
    "--interval",
    type=int,
    default=15,
    help="interval to output data at in continuous mode, in seconds (default: 15)",
)

group = parser.add_mutually_exclusive_group()

group.add_argument(
    "--no-conn-check",
    action="store_true",
    help="do not heuristically check if connected to train network",
)

group.add_argument(
    "--login",
    action="store_true",
    help="try to automatically log in to WiFi networks if connected",
)

parser.add_argument(
    "--dummy",
    choices=PROVIDERS.keys(),
    help="use dummy data of the given provider",
)

parser.add_argument(
    "--verbose", "-v", action="store_true", help="output debug information to stderr"
)

parser.add_argument(
    "--human",
    "-H",
    action="store_true",
    help="output human-readable JSON (incompatible with Waybar)",
)

args = parser.parse_args()

if args.dummy and (
    args.continuous or args.interval or args.no_conn_check or args.login
):
    parser.error(
        "--dummy mode does not provide --continuous, --interval, --no-conn-check, or --login"
    )


logging.basicConfig(
    format="%(asctime)s  [%(levelname)-8s] %(name)-13s - %(message)s",
    stream=sys.stderr,
    level=logging.DEBUG if args.verbose else logging.CRITICAL,
)
logger = logging.getLogger("waybar-trains")


if args.dummy:
    provider = PROVIDERS[args.dummy]()
    status = provider.get_dummy_status()
    if status is None:
        print(json.dumps({"text": f"provider {args.dummy} does not have dummy data"}))
        sys.exit(1)
    status.print_waybar_json()
    sys.exit(0)


# https://stackoverflow.com/a/62044100
class TimeoutHTTPAdapter(HTTPAdapter):
    def __init__(self, *args, **kwargs):
        if "timeout" in kwargs:
            self.timeout = kwargs["timeout"]
            del kwargs["timeout"]
        super().__init__(*args, **kwargs)

    def send(self, request, *args, **kwargs):
        timeout = kwargs.get("timeout")
        if timeout is None and hasattr(self, "timeout"):
            kwargs["timeout"] = self.timeout
        return super().send(request, **kwargs)


session = requests.Session()
# set a default timeout of five seconds; otherwise reconnecting the network might lead to stalling
session.mount("https://", TimeoutHTTPAdapter(timeout=5))

providers = [provider_class(session) for provider_class in PROVIDERS.values()]
while True:
    try:
        ssids = get_connected_ssids()
        status = None
        for provider in providers:
            status = provider.get_status(
                ssids, conn_check=not args.no_conn_check, login=args.login
            )
            if status is not None:
                status.print_waybar_json()
                break
        else:
            # loop did not break
            print("{}", flush=True)
    except:
        traceback.print_exc()
        print('{"text": "waybar-trains: ERROR"}', flush=True)
    if not args.continuous:
        sys.exit(0)
    time.sleep(args.interval)
